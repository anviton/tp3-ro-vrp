#ifndef __UTILS_H__
#define __UTILS_H__

#include <string.h>
#include <string>

#include "VRP-TSP.h"

#ifndef NMAX
#define NMAX 50
#endif

void			  displayMatrice				(struct T_INSTANCE instance);
void			  displayDemand					(struct T_INSTANCE instance);
void			  displayTour					(struct T_INSTANCE instance, struct T_SOLUTION solution);
void			  displayParent					(struct T_INSTANCE instance, struct T_SOLUTION solution);
void			  displayVector					(struct T_INSTANCE instance, struct T_SOLUTION solution);
void			  displayScoreHeuristic			(int scoreHeuristic[], int nbHeuristic);
void		      swapElements					(struct T_TOUR& tour, int i, int j);

bool			  isVisited						(int vertex, int visited[NMAX], int nbVisited);
struct T_INSTANCE collectInstance				(std::string filename);

void			  calculateCost					(T_SOLUTION& solution);
void			  calculateCostForOneTour		(T_INSTANCE instance, T_SOLUTION& solution, int tour);

// Remove clients from a start index to an end index
void			  suppClients					(T_TOUR& t, int indexDebut, int indexFin);

// Remove a specific client
void			  removeClient					(T_TOUR& tour, int index);

void			  addClientsFromTour			(T_TOUR& destinationTour, const T_TOUR& sourceTour, int startIndex);

void			  insertSubSequence				(T_TOUR& t1, int source[NMAX], int indexT1Debut, int indexT1Fin, int indexT2Debut, int indexT2Fin);

void			  shiftLeft						(T_TOUR& tour, int index1, int index2);
void			  shiftRight					(T_TOUR& tour, int index1, int index2);

int				  findIndexMax					(const int tab[], int taille);

#endif // __UTILS_H__