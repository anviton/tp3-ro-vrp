seed used : 0x0452, 0x1239, 0x3108, 0x4851

* optimal solution : 784

* best solution found for instance A-n32-k5 by our program : 794

------------------------------------------------------------------------------------------------
Nombre de tournee : 5
TOUR N. : 1
 -- clients :
27 22 9 8 11 4 28 18 29 15 10 25 5 20
* Cout de la tournee : 327km.

TOUR N. : 2
 -- clients :
6 23 3 2 17 19 31
* Cout de la tournee : 207km.

TOUR N. : 3
 -- clients :
26 24 14
* Cout de la tournee : 71km.

TOUR N. : 4
 -- clients :
30 1 21 13 7 16
* Cout de la tournee : 133km.

TOUR N. : 5
 -- clients :
12
* Cout de la tournee : 56km.

/=====
 * New best solution found 794 km(s).
/=====
