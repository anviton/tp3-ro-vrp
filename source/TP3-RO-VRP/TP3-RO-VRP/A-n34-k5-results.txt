seed used : 0x1952, 0x1469, 0x3178, 0x7853 


* optimal solution : 778

* best solution found for instance A-n34-k5 by our program : 796

------------------------------------------------------------------------------------------------
Nombre de tournee : 5
TOUR N. : 1
 -- clients :
1 27 23 11 19 17 31 7
* Cout de la tournee : 188km.

TOUR N. : 2
 -- clients :
33 16 22 3 12 9 2 18
* Cout de la tournee : 161km.

TOUR N. : 3
 -- clients :
21 32 28 25 13 10
* Cout de la tournee : 170km.

TOUR N. : 4
 -- clients :
14 6 15 8 29 30
* Cout de la tournee : 126km.

TOUR N. : 5
 -- clients :
20 4 26 5 24
* Cout de la tournee : 151km.

/=====
 * New best solution found 796 km(s).
/=====