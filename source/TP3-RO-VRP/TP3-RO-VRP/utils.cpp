#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "VRP-TSP.h"
#include "MersenneTwister.h"

#define NUM_TOP_DIST 7

using namespace std;



/***********************************************************
    *                                                     *
    *               DISPLAY FUNCTIONS                     *
    *                                                     *
***********************************************************/

void displayVector(T_INSTANCE instance, T_SOLUTION solution)
{
    int length = instance.nbClients;

    for (int i = 0; i <= length; i++)
    {
        cout << solution.vecteur[i] << " ";
    }
    cout << endl;
}

void displayParent(T_INSTANCE instance, T_SOLUTION solution)
{
    int length = instance.nbClients;

    for (int i = 0; i <= length; i++)
    {
        cout << "P [" << i << " ]: " << solution.parent[i] << " ";
    }
    cout << endl;
}


void displayMatrice(T_INSTANCE instance)
{
    int length = instance.nbClients;

    for (int i = 0; i <= length; i++)
    {
        for (int j = 0; j <= length; j++)
        {
            cout << instance.dist[i][j] << " ";
        }
        cout << endl;
    }
}

void displayDemand(T_INSTANCE instance)
{
    int length = instance.nbClients;
    for (int i = 1; i <= length; i++)
    {
        cout << "Client  " << i << " : " << instance.quantite[i] << endl;

    }

}

void displayTour(T_INSTANCE instance, T_SOLUTION solution)
{
    int n;
    cout << "Nombre de tournee : " << solution.nbTours << endl;
    int camion = instance.nbVehicle;

    for (int i = 0; i < solution.nbTours; i++)
    {
        cout << "TOUR N. : " << (i + 1) << endl;
        n = solution.tours[i].nbClientsDeserved;
        cout << " -- clients : " << endl;
        for (int j = 0; j < n; j++)
        {
            cout << solution.tours[i].clientsList[j] << " ";
        }
        cout << "\n* Cout de la tournee : " << solution.tours[i].cost << "km." << endl;
        cout << endl;
        camion--;
    }

}

void displayScoreHeuristic(int scoreHeuristic[], int nbHeuristic)
{
    string name[5] = { "INSERT", "2-OPT", "2-OPT INTER-TOUR", "INSERT INTER-TOUR", "REVERSE TOUR" };
    for (int i = 0; i < nbHeuristic; i++)
    {
        cout << name[i] << " : " << scoreHeuristic[i] << endl;
    }
}
/************************************************************/

/*
*
* Read an VRP instance from "filenme" file
* 
* filename : name of the source file
* 
*/
T_INSTANCE collectInstance(string filename)
{
    ifstream infile(filename);

    if (!infile)
    {
        cerr << "Error on opening file." << endl;
    }

    T_INSTANCE instance;
    string line;

    infile >> instance.nbClients;
    infile >> instance.nbVehicle;
    infile >> instance.capacityVehicle;

    int nbTown = instance.nbClients + 1;

    getline(infile, line);

    for (int i = 0; i < nbTown; i++)
    {
        getline(infile, line);
        istringstream iss(line);
        for (int j = 0; j < nbTown; j++)
        {
            iss >> instance.dist[i][j];
        }
    }

    int index;
    instance.quantite[0] = 0;

    for (int i = 1; i <= instance.nbClients; i++)
    {
        infile >> index;
        infile >> instance.quantite[i];
    }

    return instance;
}


/** isVisited()
* 
* Allows to know if the vertex is visited
* 
* vertex : vertex to verify
* visited : array of visited vertices
* nbVisited : number of vertices visited
* 
* return : true if vertex is visited else false
*/
bool isVisited(int vertex, int visited[NMAX], int nbVisited)
{
    bool v = false;
    for (int i = 0; i < nbVisited && !v; i++)
    {
        if (visited[i] == vertex)
        {
            v = true;

        }
    }
    return v;
}

/** calculateCost()
* 
* Calculate the cost of the solution 
* 
* solution : reference on an isntance of solution 
* 
*/
void calculateCost(T_SOLUTION& solution)
{
    solution.cost = 0;

    for (int k = 0; k < solution.nbTours; k++)
    {
        solution.cost += solution.tours[k].cost;
    }
}

/** calculateCostForOneTour()
* 
* Calculate the cost of a tour
* 
* instance : a VRP instance
* solution : a reference to the solution structure to be modified
* tour : position of the tour
* 
*/
void calculateCostForOneTour(T_INSTANCE instance, T_SOLUTION& solution, int tour)
{
    solution.tours[tour].cost = instance.dist[0][solution.tours[tour].clientsList[0]];

    int client;

    for (client = 0; client < solution.tours[tour].nbClientsDeserved - 1; client++)
    {
        solution.tours[tour].cost += instance.dist[solution.tours[tour].clientsList[client]][solution.tours[tour].clientsList[client + 1]];
    }

    solution.tours[tour].cost += instance.dist[solution.tours[tour].clientsList[client]][0];
}


/** suppClients()
* 
* Remove the customers between the 2 bounds of a tour
* 
* tour          : tour to be modified
* indexDebut    : index of the beginning of deletion
* indexFin      : index of the end of deletion
* 
*/
void suppClients(T_TOUR& tour, int indexDebut, int indexFin) 
{
    if (indexDebut < 0 || indexDebut >= tour.nbClientsDeserved || indexFin < indexDebut || 
        indexFin >= tour.nbClientsDeserved)
    {
        return;
    }

    for (int i = indexDebut; i <= indexFin; ++i) 
    {
        tour.clientsList[i] = tour.clientsList[i + indexFin - indexDebut + 1];
    }

    tour.nbClientsDeserved -= indexFin - indexDebut + 1;
}

/** addClientsFromTour()
* 
* Add clients to a tour
* 
* destinationTour : destination tour of the vertices
* 
* 
*/
void addClientsFromTour(T_TOUR& destinationTour, const T_TOUR& sourceTour, int startIndex)
{
    if (startIndex >= 0 && startIndex <= sourceTour.nbClientsDeserved)
    {

        int clientsToAdd = sourceTour.nbClientsDeserved - startIndex;

        for (int i = 0; i < clientsToAdd; ++i)
        {
            destinationTour.clientsList[destinationTour.nbClientsDeserved] = sourceTour.clientsList[startIndex + i];
            destinationTour.nbClientsDeserved++;
        }
    }
}

/** removeClient()
* 
* Remove a client from a tour
* 
* tour : structure that represents a tour
*
*/
void removeClient(T_TOUR& tour, int index)
{
    if (tour.nbClientsDeserved > 0 && index >= 0 && index <= tour.nbClientsDeserved)
    {
        for (int i = index; i < tour.nbClientsDeserved - 1; i++)
        {
            tour.clientsList[i] = tour.clientsList[i + 1];
        }

        tour.nbClientsDeserved--;
    }
}


/** insertSubSequence()
* 
* Function to insert a subsequence from source into a T_TOUR structure
* 
* t1            : represents a reference to an object of type 'T_TOUR'
* source        : array of integers named 'source'
* indexT1Debut  : starting indice of t1
* indexT1Fin    : ending indice of t1
* indexT2Debut  : starting indice of t2
* indexT2Fin    : ending indice of t2
*/
void insertSubSequence(T_TOUR& t1, int source[NMAX], int indexT1Debut, int indexT1Fin, int indexT2Debut, int indexT2Fin)
{
    int tailleSousSequenceT1 = indexT1Fin - indexT1Debut + 1;
    int tailleSousSequenceT2 = indexT2Fin - indexT2Debut + 1;


    if (t1.nbClientsDeserved + tailleSousSequenceT2 - tailleSousSequenceT1 >= NMAX) 
    {
        return;
    }

    shiftRight(t1, indexT1Debut, indexT1Fin);

    int indexT1 = indexT1Debut;
    for (int i = indexT2Debut; i <= indexT2Fin; ++i) {
        t1.clientsList[indexT1] = source[i];
        ++indexT1;
    }
    t1.nbClientsDeserved += tailleSousSequenceT2 - tailleSousSequenceT1;
}

/** shiftLeft()
* 
* Shifts the vertices to the left from vertex at index 1 to vertex at index 2
* 
* tour   : tour to be modified
* index1 : index to start the shift
* index2 : ending index of the shift
*/
void shiftLeft(T_TOUR& tour, int index1, int index2)
{
    if (tour.nbClientsDeserved > 0 && index1 >= 0 && index2 < tour.nbClientsDeserved && index1 <= index2)
    {
        int firstElement = tour.clientsList[index1];

        for (int i = index1; i < index2; ++i)
        {
            tour.clientsList[i] = tour.clientsList[i + 1];
        }

        tour.clientsList[index2] = firstElement;
    }
}


/** shiftRight()
*
* Shifts the vertices to the right from vertex at index 1 to vertex at index 2
*
* tour   : tour to be modified
* index1 : index to start the shift
* index2 : ending index of the shift
*/
void shiftRight(T_TOUR& tour, int index1, int index2)
{
    if (tour.nbClientsDeserved > 0 && index1 >= 0 && index2 <= tour.nbClientsDeserved && index1 <= index2)
    {
        int lastElement = tour.clientsList[index2];

        for (int i = index2; i > index1; --i)
        {
            tour.clientsList[i] = tour.clientsList[i - 1];
        }

        tour.clientsList[index1] = lastElement;
    }
}

/** swapElements()
* 
* Swap the position of 2 vertices in a tour
* 
* tour : reference on an instance of a tour to be modified
* i    : position of a vertex
* j    : poisition of a vertex
* 
*/
void swapElements(T_TOUR& tour, int i, int j)
{
    i++;

    while (i < j)
    {
        int temp = tour.clientsList[i];
        tour.clientsList[i] = tour.clientsList[j];
        tour.clientsList[j] = temp;
        i++;
        j--;
    }
}

/** findIndexMax()
* 
* Function to find the index of the maximum element in an array
* 
* tab    : array in which the function will search for the maximum element
* taille : size/length of the array 'tab' 
*/
int findIndexMax(const int tab[], int taille) 
{
    int max      = tab[0];
    int indexMax = 0;

    for (int i = 1; i < taille; ++i) {
        if (tab[i] > max) 
        {
            max      = tab[i];
            indexMax = 0;
        }
    }
    return indexMax;
}



