// TP3-RO-VRP.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>

#include "VRP-TSP.h"
#include "utils.h"


#define MAX_ITER_GRASP 200

using namespace std;

int main()
{
	// Load the VRP instance
	T_INSTANCE instance[NMAX];
	
	int nbVisited = 0;

	// Seed used for MT repeatability
	uint32_t init[4] = { 0x0452, 0x1239, 0x3108, 0x4851 }, length = 4;

	// Creation of the Mersenne Twister generator
	MersenneTwister* mt = new MersenneTwister(init, length);
	

	// Uncomment to generate a solution 

	/*T_SOLUTION sol;

	//nearestNeighbor(instance, sol);
	randomizedNearestNeighbor(instance, sol, 0, mt);
	*/
	

	// Uncomment to test a heuristic
	/*cout << "/====================================\n * Cout total de la solution : " << sol.cost << " km(s). \n/====================================\n" << endl;

	cout << " --> INSERT INTER-TOUR\n" << endl;

	// Change the heuristic here
	twoOptInterTour(instance, sol, 0, 1);

	for (int tour = 0; tour < sol.nbTours; tour++)
	{
		calculateCostForOneTour(instance, sol, tour);
	}

	calculateCost(sol);

	displayTour(instance, sol);

	cout << "/====================================\n * Cout total de la solution : " << sol.cost << " km(s). \n/====================================\n" << endl;*/


	// GRASP - CRVP-Lib + Paris
	std::string filesname[] = { "VRP-paris.txt", "A-n32-k5.txt", "A-n33-k5.txt", "A-n34-k5.txt", "A-n36-k5.txt", "A-n37-k5.txt", "A-n38-k5.txt" };

	for (const std::string& filename : filesname)
	{
		// Collect the instance from 'filename'
		T_INSTANCE instance = collectInstance(filename);

		std::cout << "\n------------\n* Instance : " + filename << "\n------------\n" << std::endl;

		// GRASP on the 'instance'
		GRASP(instance, mt, MAX_ITER_GRASP);
	}

	return 0;
}
