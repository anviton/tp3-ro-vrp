#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <ctime>

#include "VRP-TSP.h"
#include "MersenneTwister.h"

// Number of top distances to consider in the nearestNeighbor algorithm
#define NUM_TOP_DIST 7

// Number of heuristics
#define NB_HEURISTIC 5

// Array to store the score of each heuristic
int scoreHeuristic[NB_HEURISTIC] = { 0, 0, 0, 0, 0 };

// Array to store the score of each heuristic
double prob[]                    = { 0.20, 0.40, 0.60, 0.80, 1 };

using namespace std;



/** nearestNeighbor()
 * 
 * Implements the nearest neighbor heuristic to construct an initial solution for VRP-TW.
 *
 * instance : a VRP instance
 * solution : reference to the solution structure to be modified.
 */

void nearestNeighbor(T_INSTANCE instance, T_SOLUTION& solution)
{
    // Array to store the nearest neighbors
    int list[7];

    int newPos, tmp, k;
    int nbEltList;
    int nbVisited;
    double random;

    nbVisited = 0;

    int visited[NMAX] = { -1 };
    visited[nbVisited] = 0;

    nbEltList = 0;
    nbVisited++;

    // Starting from the depot
    solution.vecteur[0] = 0;

    for (int i = 1; i <= instance.nbClients; i++)
    {
        nbEltList = 0;

        // Find the nearest neighbors for the current customer
        for (int j = 0; j <= instance.nbClients; j++)
        {
            if (instance.dist[i][j] != 0 && !isVisited(j, visited, nbVisited))
            {
                list[nbEltList] = j;
                newPos = nbEltList;

                // Insert the neighbor in non-decreasing order of distances
                while (newPos > 0 && instance.dist[i][j] < instance.dist[i][list[newPos - 1]])
                {
                    tmp = list[newPos - 1];
                    list[newPos - 1] = list[newPos];
                    list[newPos] = tmp;
                    newPos--;
                }

                // Keep only the top neighbors
                if (nbEltList < NUM_TOP_DIST - 1)
                {
                    nbEltList++;
                }
            }
        }

        // Select the nearest neighbor and update the solution
        solution.vecteur[i] = list[0];

        visited[nbVisited] = solution.vecteur[i];

        nbVisited++;
    }
}


/** randomizedNearestNeighbor()
 * 
 * Implements the randomized nearest neighbor heuristic to construct an initial solution for VRP-TW.
 *
 * instance : a VRP instance.
 * solution : reference to the solution structure to be modified.
 * nbVisited: the number of visited nodes.
 * mt       : pointer to the Mersenne Twister random number generator.
 */

void randomizedNearestNeighbor(T_INSTANCE instance, T_SOLUTION& solution, int nbVisited, MersenneTwister* mt) {

    double listProbaCumu[6] = { 0.02, 0.095, 0.175, 0.3, 0.5, 1 };

    int list[7];
    int newPos, tmp, lv, k;
    int nbEltList;

    double random;

    int visited[NMAX] = { -1 };
    visited[nbVisited] = 0;
    nbEltList = 0;

    nbVisited++;


    solution.vecteur[0] = 0;

    for (int i = 1; i <= instance.nbClients; i++)
    {
        nbEltList = 0;

        // Find the nearest neighbors for the current customer
        for (int j = 0; j <= instance.nbClients; j++)
        {
            if (instance.dist[i][j] != 0 && !isVisited(j, visited, nbVisited))
            {
                list[nbEltList] = j;
                newPos = nbEltList;

                // Insert the neighbor in non-decreasing order of distances
                while (newPos > 0 && instance.dist[i][j] < instance.dist[i][list[newPos - 1]])
                {
                    tmp = list[newPos - 1];
                    list[newPos - 1] = list[newPos];
                    list[newPos] = tmp;
                    newPos--;
                }

                // Keep only the top neighbors
                if (nbEltList < NUM_TOP_DIST - 1)
                {
                    nbEltList++;
                }
            }
        }

        random = mt->genrand_real1();
        k = 0;
        
        // Choose a neighbor based on cumulative probability and check for visited status
        while ((random > listProbaCumu[k] && k < (NUM_TOP_DIST - 3)) || (isVisited(list[(5 - k)], visited, nbVisited)))
        {
            k++;
            if (!(isVisited(list[(5 - k)], visited, nbVisited)))
            {
                lv = k;
            }
        }

        solution.vecteur[i] = list[((NUM_TOP_DIST - 2) - k)];

        visited[nbVisited] = solution.vecteur[i];
        nbVisited++;
    }
}

/** splitCVRP()
* 
* Consider limited number of vehicules for VRP instance
* 
* instance : VRP instance
* solution : reference to the solution structure to be modified
*/

void splitCVRP(T_INSTANCE instance, T_SOLUTION& solution)
{
    int capacity;
    int client;
    int cost = 0;
    int currentClient;
    int previousClient;

    solution.cost = 0;

    // Initialize solution
    for (int i = 0; i <= instance.nbClients; i++)
    {
        solution.date[i] = MAX;
        solution.parent[i] = -1;
    }

    solution.date[0] = 0;

    T_LABEL label;

    label.nbVehiculeAvailable = instance.nbVehicle;

    for (int i = 0; i < instance.nbClients; i++)
    {
        // Update capacity and index of client
        capacity = 0;
        client = i + 1;

        // Check for posibility to deserve this client
        while (client <= instance.nbClients && (capacity + instance.quantite[solution.vecteur[client]] < instance.capacityVehicle) 
            && label.nbVehiculeAvailable > 0)
        {
            currentClient = solution.vecteur[client];
            previousClient = solution.vecteur[client - 1];

            // Only one client in the tours
            if (i + 1 == client)
            {
                // Round trip to currentClient and departure point
                cost = instance.dist[0][currentClient] + instance.dist[currentClient][0];
                capacity = instance.quantite[currentClient];
            }
            else
            {
                cost += instance.dist[currentClient][0] + instance.dist[previousClient][currentClient] - instance.dist[previousClient][0];
                capacity += instance.quantite[currentClient];

            }

            if (solution.date[i] + cost < solution.date[client])
            {
                solution.date[client] = solution.date[i] + cost;
                solution.parent[client] = i;
            }

            client++;
        }

        label.nbVehiculeAvailable--;

    }
    //displayParent(instance, solution);
}


/** evalue()
 * 
 * evaluates the solution using the SPLIT delivery strategy.
 *
 * instance : a VRP instance.
 * solution : reference to the solution structure to be evaluated.
 */

void evalue(T_INSTANCE instance, T_SOLUTION& solution)
{
    int capacity;
    int client;
    int cost = 0;
    int currentClient;
    int previousClient;

    solution.cost = 0;

    // Initialize solution
    for (int i = 0; i <= instance.nbClients; i++)
    {
        solution.date[i] = MAX;
        solution.parent[i] = -1;
    }

    solution.date[0] = 0;

    for (int i = 0; i < instance.nbClients; i++)
    {
        // Update capacity and index of client
        capacity = 0;
        client = i + 1;

        // Check for posibility to deserve this client
        while (client <= instance.nbClients && (capacity + instance.quantite[solution.vecteur[client]] < instance.capacityVehicle))
        {
            currentClient = solution.vecteur[client];
            previousClient = solution.vecteur[client - 1];

            // Only one client in the tours
            if (i + 1 == client)
            {
                // Round trip to currentClient and departure point
                cost = instance.dist[0][currentClient] + instance.dist[currentClient][0];
                capacity = instance.quantite[currentClient];
            }
            else
            {
                cost += instance.dist[previousClient][currentClient] + instance.dist[currentClient][0] - instance.dist[previousClient][0];
                capacity += instance.quantite[currentClient];

            }

            if (solution.date[i] + cost < solution.date[client])
            {
                solution.date[client] = solution.date[i] + cost;
                solution.parent[client] = i;
            }

            client++;
        }
    }
}


/** determineTour()
 * 
 * Determines the number of tours in the solution and calculates the cost for each tour.
 *
 * instance : a VRP instance.
 * solution : reference to the solution structure to be processed.
 */

void determineTour(T_INSTANCE& instance, T_SOLUTION& solution)
{
    int tour            = 0;
    int noParent        = -1;
    int t, pt;
    int previousTour    = 0;

    int nbClients       = instance.nbClients + 1;

    int cost            = 0;
    int maxCost         = 0;
    int totalCost       = 0;


    solution.nbTours = 0;

    for (int i = 1; i < nbClients; i++)
    {
        t = solution.parent[i];
        pt = solution.vecteur[i];

        // If not the same tour
        if (t != previousTour)
        {
            if (tour != 0)
            {
                calculateCostForOneTour(instance, solution, tour);
            }
            else
            {
                solution.tours[tour].cost = cost;
            }

            // Move to a new tour
            tour++;
            solution.tours[tour].capacity[0]       = 0;
            solution.tours[tour].nbClientsDeserved = 0;
            solution.tours[tour].clientsList[0]    = 0;


            maxCost = 0;
        }

         // If the same tour <=> same parent, then consider the new client
        int n = solution.tours[tour].nbClientsDeserved++;

        // Add the client to the tour
        solution.tours[tour].clientsList[n] = pt;
        previousTour = t;

        cost = solution.date[i];
    }

    calculateCostForOneTour(instance, solution, tour);
    solution.nbTours = ++tour;
    calculateCost(solution);
}


/** splitInv()
 *  
 * Applies the split inverse operation on a given solution.
 *
 *  instance : a VRP instance.
 *  solution : a reference to the solution structure to be modified.
 * 
 * Return a TSP solution using the VRP instance & solution
 */

T_SOLUTION splitInv(T_INSTANCE instance, T_SOLUTION& solution)
{
    T_SOLUTION newSolution;

    int nbTours = solution.nbTours;
    int k = 0;

    int nbClients;

    newSolution.vecteur[k] = 0; // d�p�t
    k++;

    for (int i = 0; i <= nbTours; i++)
    {

        nbClients = solution.tours[i].nbClientsDeserved;

        for (int j = 0; j < nbClients; j++)
        {
            newSolution.vecteur[k] = solution.tours[i].clientsList[j];
            k++;
        }
    }

    return newSolution;
}


/***********************************************************
    *                                                     *
    *                   HEURISTIQUES                      *
    *                                                     *
***********************************************************/



/** insert()
 * 
 * Inserts a client into a specific tour to optimize the solution.
 *
 * instance : a VRP instance.
 * solution : a reference to the solution structure to be modified.
 * posTournee : position of the tour where the client is to be inserted.
 */

void insert(T_INSTANCE instance, T_SOLUTION& solution, int posTournee)
{
    T_TOUR& tour = solution.tours[posTournee];

    int deltaMinus, deltaPlus;
    int nbClients = tour.nbClientsDeserved;

    int savePlaceToInsert = -1;
    int saveClient = -1;


    int prevClientToDeplace;
    int suivClientToDeplace;
    int currentClient;

    deltaMinus = 0;
    deltaPlus = 0;

    int index = 0;

    int bestMove = 0;

    for (int i = 0; i < nbClients; i++)
    {
        prevClientToDeplace = (i == 0 ? 0 : tour.clientsList[i - 1]);
        suivClientToDeplace = (i == nbClients - 1 ? 0 : tour.clientsList[i + 1]);
        currentClient = tour.clientsList[i];

        deltaMinus = -instance.dist[prevClientToDeplace][currentClient] - instance.dist[currentClient][suivClientToDeplace];
        deltaPlus = instance.dist[prevClientToDeplace][suivClientToDeplace];

        for (int j = 0; j < nbClients; j++)
        {
            int p = (j == 0 ? 0 : tour.clientsList[j]);
            int s = (j == nbClients - 1 ? 0 : tour.clientsList[j + 1]);

            if (p != currentClient && s != currentClient)
            {
                deltaPlus += instance.dist[p][currentClient] + instance.dist[currentClient][s];
                deltaMinus -= instance.dist[p][s];

                if (deltaMinus + deltaPlus < bestMove)
                {
                    savePlaceToInsert = j;
                    saveClient = i;
                    bestMove = deltaMinus + deltaPlus;
                }
            }
        }
    }

    if (savePlaceToInsert != -1 && saveClient != -1)
    {
        if (savePlaceToInsert > saveClient)
        {
            shiftLeft(tour, saveClient, savePlaceToInsert);
        }
        else
        {
            shiftRight(tour, savePlaceToInsert, saveClient);
        }
    }
}

/** insertInterTour()
 * 
 * Inserts a client from one tour into another to optimize the solution.
 *
 * instance    : a VRP instance.
 * solution    : a reference to the solution structure to be modified.
 * posTournee1 : a position of the first tour.
 * posTournee2 : a position of the second tour.
 */

void insertInterTour(T_INSTANCE instance, T_SOLUTION& solution, int posTournee1, int posTournee2)
{
    T_TOUR& tour1 = solution.tours[posTournee1];
    T_TOUR& tour2 = solution.tours[posTournee2];

    int prevClientToDeplace;
    int suivClientToDeplace;
    int currentClient;

    int deltaMinus = 0;
    int deltaPlus = 0;

    int savePlaceToInsert = -1;
    int saveClient        = -1;

    int bestMove = 0;

    for (int i = 0; i < tour1.nbClientsDeserved; i++)
    {
        prevClientToDeplace = (i == 0 ? 0 : tour1.clientsList[i - 1]);
        suivClientToDeplace = (i == tour1.nbClientsDeserved - 1 ? 0 : tour1.clientsList[i + 1]);
        currentClient = tour1.clientsList[i];

        deltaMinus = -instance.dist[prevClientToDeplace][currentClient] - instance.dist[currentClient][suivClientToDeplace];
        deltaPlus = instance.dist[prevClientToDeplace][suivClientToDeplace];

        for (int j = 0; j < tour2.nbClientsDeserved; j++)
        {
            int p = (j == 0 ? 0 : tour2.clientsList[j]);
            int s = (j == tour2.nbClientsDeserved - 1 ? 0 : tour2.clientsList[j + 1]);

            deltaMinus = -instance.dist[p][s];
            deltaPlus = instance.dist[p][currentClient] + instance.dist[currentClient][s];

            if (deltaMinus + deltaPlus < bestMove)
            {
                savePlaceToInsert = j;
                saveClient = i;
                bestMove = deltaPlus + deltaPlus;
            }
        }

    }

    if (savePlaceToInsert != -1 && saveClient != -1)
    {
        int clientDeplacedInTour1 = tour1.clientsList[saveClient];
        removeClient(tour1, saveClient);
        shiftRight(tour2, savePlaceToInsert + 1, tour2.nbClientsDeserved );
        tour2.clientsList[savePlaceToInsert + 1] = clientDeplacedInTour1;
        tour2.nbClientsDeserved++;
    }
}


/** twoOpt()
 * 
 * Applies the Two-OPT intra-tour optimization to improve the solution.
 *
 * instance   : a VRP instance.
 * solution   : a reference to the solution structure to be modified.
 * posTournee : position of the tour to optimize.
 */

void twoOpt(T_INSTANCE instance, T_SOLUTION& solution, int posTournee)
{
    int iter = 0;
    int delta;
    int i, j;

    int pi = -1;
    int pj = -1;

    T_TOUR& tour = solution.tours[posTournee];

    int nbClients = solution.tours[posTournee].nbClientsDeserved;

    for (i = 0; i < tour.nbClientsDeserved; i++)
    {
        for (j = i + 2; j < tour.nbClientsDeserved; j++)
        {
            int next = j == tour.nbClientsDeserved - 1 ? 0 : tour.clientsList[j + 1];

            delta = -instance.dist[tour.clientsList[i]][tour.clientsList[i + 1]] - instance.dist[tour.clientsList[j]][next]
                + instance.dist[tour.clientsList[i]][tour.clientsList[j]] + instance.dist[tour.clientsList[i + 1]][next];

            if (delta < 0)
            {
                pi = i;
                pj = j;
            }

        }
    }

    if (pi != -1 && pj != -1)
    {
        swapElements(tour, pi, pj);
    }
}

/** twoOptInterTour()
 * Applies the Two-OPT inter-tour optimization to improve the solution.
 *
 * instance    : a VRP instance.
 * solution    : a reference to the solution structure to be modified.
 * posTournee1 : a position of the first tour.
 * posTournee2 : a position of the second tour.
 */

void twoOptInterTour(T_INSTANCE instance, T_SOLUTION& solution, int posTournee1, int posTournee2)
{
    T_TOUR& tour1 = solution.tours[posTournee1];
    T_TOUR& tour2 = solution.tours[posTournee2];

    int nbClientsTour1 = tour1.nbClientsDeserved;
    int nbClientsTour2 = tour2.nbClientsDeserved;

    int deltaPlus = 0;
    int deltaMinus = 0;

    int previousClientToDeplaceInTour1;
    int suivClientToDeplaceInTour1;

    int previousClientToDeplaceInTour2;
    int suivClientToDeplaceInTour2;

    int saveClientTour1 = -1;
    int saveClientTour2 = -1;
    int indexTour1 = -1;
    int indexTour2 = -1;

    for (int i = 1; i < nbClientsTour1 - 1; i++)
    {

        previousClientToDeplaceInTour1 = tour1.clientsList[i];
        suivClientToDeplaceInTour1 = (i == nbClientsTour1 - 1) ? 0 : tour1.clientsList[i + 1];

        deltaMinus = -instance.dist[previousClientToDeplaceInTour1][suivClientToDeplaceInTour1];

        for (int j = 1; j < nbClientsTour2 - 1; j++)
        {

            previousClientToDeplaceInTour2 = tour2.clientsList[j];
            suivClientToDeplaceInTour2 = (j == nbClientsTour2 - 1) ? 0 : tour2.clientsList[j + 1];

            deltaMinus = -instance.dist[previousClientToDeplaceInTour2][suivClientToDeplaceInTour2];

            deltaPlus = instance.dist[previousClientToDeplaceInTour1][suivClientToDeplaceInTour2]
                + instance.dist[previousClientToDeplaceInTour2][suivClientToDeplaceInTour1];

            if (deltaMinus + deltaPlus < 0)
            {
                saveClientTour1 = i;
                saveClientTour2 = j;

            }
        }
    }

    if (saveClientTour1 != -1 && saveClientTour2 != -1)
    {
       // cout << "Inter-tour entre " << saveClientTour1 << " et " << saveClientTour2 << endl;
        int clientsToDeplaceT1[NMAX];
        int clientsToDeplaceT2[NMAX];

        int index1 = 0;
        int index2 = 0;

        // Number of clients in tour 1 to deplace in tour 2 
        int k1 = 0;

        for (index1 = saveClientTour1 + 1; index1 < tour1.nbClientsDeserved; index1++)
        {
            clientsToDeplaceT1[k1] = tour1.clientsList[index1];
            k1++;
        }

        tour1.nbClientsDeserved -= k1;

        // Number of clients in tour 2 to deplace in tour 1 
        int k2 = 0;

        for (index2 = saveClientTour2 + 1; index2 < tour2.nbClientsDeserved; index2++)
        {
            clientsToDeplaceT2[k2] = tour2.clientsList[index2];
            k2++;
        }

        tour2.nbClientsDeserved -= k2;

        suppClients(tour1, saveClientTour1 + 1, saveClientTour1 + k1);
        suppClients(tour2, saveClientTour2 + 1, saveClientTour2 + k2);

        int k = 0;

        while (k1 != 0)
        {
            tour2.clientsList[tour2.nbClientsDeserved] = clientsToDeplaceT1[k];
            k1--;
            k++;
            tour2.nbClientsDeserved++;
        }

        k = 0;

        while (k2 != 0)
        {
            tour1.clientsList[tour1.nbClientsDeserved] = clientsToDeplaceT2[k];
            k2--;
            k++;
            tour1.nbClientsDeserved++;
        }

    }
}

/** reverseRandomTour()
 * 
 * Reverses the order of clients in a randomly selected tour of the solution.
 *
 * solution : a reference to the solution structure to be modified.
 */

void reverseRandomTour(T_SOLUTION& solution) 
{
    if (solution.nbTours <= 1) return;

    // Generate a random tour index to reverse
    int randomTourIndex = rand() % solution.nbTours;
    T_TOUR& selectedTour = solution.tours[randomTourIndex];

    // Reverse the order of clients in the selected tour
    for (int i = 0; i < selectedTour.nbClientsDeserved / 2; ++i)
    {
        swap(selectedTour.clientsList[i], selectedTour.clientsList[selectedTour.nbClientsDeserved - 1 - i]);
    }

}


/** localSearchTSP()
 * 
 * Applies a local search heuristic to improve the given TSP solution.
 *
 * instance : a TSP instance.
 * solution : a reference to the solution structure to be improved.
 * mt       : a pointer to the Mersenne Twister random number generator.
 * maxIter  : maximum number of iterations for the local search.
 * 
 * return  : the index of the applied heuristic (0: Insert, 1: 2-Opt, 2: Insert-Inter-Tour, 3: 2-Opt-Inter-Tour, 4: Reverse Random Tour).
 */

int localSearchTSP(T_INSTANCE instance, T_SOLUTION& solution, MersenneTwister* mt, int maxIter)
{
    int iter = 0;
    int heuristic;

    double randProb;

    int posTour, posTour1, posTour2;

    while (iter < maxIter)
    {
        randProb = mt->genrand_real1();

        if (randProb < prob[0])
        {
            // Insertion
            posTour = mt->genrand_int31() % solution.nbTours;
            insert(instance, solution, posTour);

            heuristic = 0;
        }
        else if (randProb < prob[1])
        {
            // 2-OPT
            posTour = mt->genrand_int31() % solution.nbTours;
            twoOpt(instance, solution, posTour);

            heuristic = 1;

        }
        else if (randProb < prob[2])
        {
            // Insert Inter-Tour
            posTour1 = mt->genrand_int31() % solution.nbTours;
            posTour2 = mt->genrand_int31() % solution.nbTours;
            while (posTour1 == posTour2)
            {
                posTour2 = mt->genrand_int31() % solution.nbTours;
            }
            insertInterTour(instance, solution, posTour1, posTour2);

            heuristic = 2;

        }
        else if (randProb < prob[3])
        {
            // 2-OPT Inter-Tour
            posTour1 = mt->genrand_int31() % solution.nbTours;
            posTour2 = mt->genrand_int31() % solution.nbTours;

            while (posTour1 == posTour2)
            {
                posTour2 = mt->genrand_int31() % solution.nbTours;
            }
            twoOptInterTour(instance, solution, posTour1, posTour2);

            heuristic = 3;
        }
        else
        {
            // Reverse Random Tour
            reverseRandomTour(solution);

            heuristic = 4;
        }

        for (int t = 0; t < solution.nbTours; t++)
        {
            calculateCostForOneTour(instance, solution, t);
        }

        calculateCost(solution);

        iter++;
    }

    return heuristic;
}



/** localSearchVRP()
 * Applies a local search heuristic to improve the given VRP solution.
 *
 * instance    : a VRP instance.
 * solutionVRP : a reference to the VRP solution structure to be improved.
 * mt          : a pointer to the Mersenne Twister random number generator.
 * maxIter     : maximum number of iterations for the local search.
 * 
 * return      : the best VRP solution found after local search.
 */

T_SOLUTION localSearchVRP(T_INSTANCE instance, T_SOLUTION& solutionVRP, MersenneTwister* mt, int maxIter)
{
    int iter = 0;
    int maxIterLS = 1;

    int bestCost  = INT_MAX;

    int heuristic = 0;

    T_SOLUTION bestSolLS;

    while (iter < maxIter)
    {
        // Evaluate and determine tours for the current solution
        evalue(instance, solutionVRP);
        determineTour(instance, solutionVRP);

        // Apply TSP local search
        heuristic = localSearchTSP(instance, solutionVRP, mt, maxIterLS);
        

        T_SOLUTION newTSP;

        // Update best solution if a better one is found
        if (bestCost > solutionVRP.cost) 
        {
            bestCost = solutionVRP.cost;
            bestSolLS = solutionVRP;

            scoreHeuristic[heuristic]++;
        }

        // Apply split inverse operation to the current solution
        newTSP = splitInv(instance, solutionVRP);

        solutionVRP = newTSP;
        iter++;
    }

    return bestSolLS;
}


/** GRASP()
 * 
 * Executes the GRASP (Greedy Randomized Adaptive Search Procedure) algorithm to find a solution for the VRP.
 *
 * instance : a VRP instance.
 * mt       : a pointer to the Mersenne Twister random number generator.
 * maxIter  : maximum number of iterations for the GRASP algorithm.
 */

void GRASP(T_INSTANCE instance, MersenneTwister* mt, int maxIter)
{
    T_SOLUTION bestSolutionFind;
    bestSolutionFind.cost = INT_MAX;


    int iter = 0;
    int bestHeuristic;

    while (iter < maxIter)
    {
        T_SOLUTION solution;

        double rand = mt->genrand_real1();

        if (rand < 0.9)
        {
            randomizedNearestNeighbor(instance, solution, 0, mt);
        }
        else
        {
            nearestNeighbor(instance, solution);
        }

        solution = localSearchVRP(instance, solution, mt, 4500);
       

        if (bestSolutionFind.cost > solution.cost)
        {
            // displayTour(instance, solution);

            cout << "-> New best solution found : " << solution.cost << " km(s).\n" << endl;

            bestSolutionFind = solution;

            //displayScoreHeuristic(scoreHeuristic, NB_HEURISTIC);
        }

        clearScoreHeuristic();

        iter++;
    }

    cout << "/=====\n * BEST solution found " << bestSolutionFind.cost << " km(s). \n/=====\n" << endl;
    displayTour(instance, bestSolutionFind);
    cout << "/=========================================================================================\n" << endl;
}

/** clearScoreHeuristic()
 * 
 * Clears the score heuristic array by setting all values to 0.
 */

void clearScoreHeuristic()
{
    for (int h = 0; h < NB_HEURISTIC; h++)
    {
        scoreHeuristic[h] = 0;
    }
}
