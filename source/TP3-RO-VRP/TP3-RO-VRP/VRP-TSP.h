#ifndef __TLS_H__
#define __TLS_H__

#include <string.h>
#include <string>
#include<vector>

#include "utils.h"
#include "MersenneTwister.h"

#define MAX  9999999
#define NMAX 50
#define MMAX 50


typedef struct T_LABEL
{
	int nbVehiculeAvailable;
	int cost;
	int parent;
	int pos;

}T_LABEL;


typedef struct T_INSTANCE
{
	int nbClients; // Number of clients
	int nbVehicle; // Number of vehicles
	int capacityVehicle; // Capacity of vehicles
	int dist    [NMAX][MMAX]; // Distance between each cities
	int quantite[NMAX]; // Quantity asked by each clients

}T_INSTANCE;


typedef struct T_TOUR
{
	int nbClientsDeserved = 0; // Number of clients deserved during the tour
	int cost; // Cost of the tour
	int clientsList[NMAX]; // List of clients deserved
	int capacity[NMAX]; // Cumulative list of capacity

}T_TOUR;

typedef struct T_SOLUTION
{
	int    nbTours; // Number of tours
	T_TOUR tours  [NMAX]; // List of tours
	int    date   [NMAX]; // Date of arrival for each clients
	int    parent [NMAX];
	int    cost;		  // Cost
	int    vecteur[NMAX]; // TSP solution

}T_SOLUTION;



void		splitCVRP					(T_INSTANCE instance, T_SOLUTION& solution);
void		nearestNeighbor				(T_INSTANCE instance, T_SOLUTION& solution);
void		randomizedNearestNeighbor   (T_INSTANCE instance, T_SOLUTION &solution, int nbVisited, MersenneTwister *mt);

void		evalue						(T_INSTANCE instance,  T_SOLUTION& solution);
void		determineTour				(T_INSTANCE& instance, T_SOLUTION& solution);

void        insert						(T_INSTANCE instance, T_SOLUTION& solution, int posTour);
void        insertInterTour				(T_INSTANCE instance, T_SOLUTION& solution, int posTournee1, int posTournee2);
void        twoOpt						(T_INSTANCE instance, T_SOLUTION& solution, int posTour);
void	    twoOptInterTour				(T_INSTANCE instance, T_SOLUTION& solution, int posTournee1, int posTournee2);

T_SOLUTION  splitInv					(T_INSTANCE instance, T_SOLUTION& solution);


T_SOLUTION localSearchVRP				(T_INSTANCE instance, T_SOLUTION& solutionVRP, MersenneTwister* mt, int maxIter);
int 	   localSearchTSP				(T_INSTANCE instance, T_SOLUTION& solution,	   MersenneTwister* mt, int maxIter);
void	   GRASP						(T_INSTANCE instance, MersenneTwister* mt, int maxIter);


void	   clearScoreHeuristic			(void);
#endif __TLS_H__
