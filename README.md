# ``TP3 - Vehicule Routing Problem solver (VRP)``

Ce projet est une implémentation en C++ de différents algorithmes de résolution du Problème de Routage de Véhicules (VRP). Il propose des solutions efficaces pour optimiser le trajet des véhicules de livraison, minimisant la distance totale parcourue tout en respectant les contraintes de capacité de chaque véhicule.

## Fonctionnalités

- Implémentation de l'algorithme du Plus Proche Voisin Randomisé pour générer une solution initiale.
- Mise en place des heuristiques d'optimisation de solution pour la recherche locale (2-OPT / Insertion)
- Autre heuristique disponible : Reverse Tour (renverser une tournée du VRP)
- Mise en place du GRASP

# Test des instances CVRP-LIB

Ce dépôt de code a été utilisé pour tester différentes instances de la Capacitated Vehicle Routing Problem (CVRP) en utilisant une bibliothèque spécifique. Les résultats des tests sont résumés ci-dessous.

## Instances testées

Les instances de la CVRP utilisées pour les tests sont les suivantes :

- [A-n32-k5](http://vrp.galgos.inf.puc-rio.br/media/com_vrp/instances/A/A-n32-k5.vrp) : 32 clients, 5 camions de capacité 100
- [A-n33-k5](http://vrp.galgos.inf.puc-rio.br/media/com_vrp/instances/A/A-n33-k5.vrp) : 33 clients, 5 camions de capacité 100
- [A-n34-k5](http://vrp.galgos.inf.puc-rio.br/media/com_vrp/instances/A/A-n34-k5.vrp) : 34 clients, 5 camions de capacité 100
- [A-n36-k5](http://vrp.galgos.inf.puc-rio.br/media/com_vrp/instances/A/A-n36-k5.vrp) : 36 clients, 5 camions de capacité 100
- [A-n37-k5](http://vrp.galgos.inf.puc-rio.br/media/com_vrp/instances/A/A-n37-k5.vrp) : 37 clients, 5 camions de capacité 100
- [A-n38-k5](http://vrp.galgos.inf.puc-rio.br/media/com_vrp/instances/A/A-n38-k5.vrp) : 38 clients, 5 camions de capacité 100

## Résultats

Les résultats des tests sur chaque instance sont présentés dans le tableau ci-dessous :

| Instance            | Meilleur coût attendu | Coût obtenu |
|---------------------|---------------------- |-------------|
| A-n32-k5            | 784                   | 794         |
| A-n33-k5            | 661                   | 668         |
| A-n34-k5            | 778                   | 796         |
| A-n36-k5            | 799                   | 801         |
| A-n37-k5            | 669                   | 698         |
| A-n38-k5            | 730                   | 755         |


## Références

- [Bibliothèque CVRP utilisée](http://vrp.galgos.inf.puc-rio.br/index.php/en/)

# Structures de données pour la résolution du Problème de Routage de Véhicules (VRP)

## ``T_LABEL``

Structure représentant une étiquette utilisée dans l'algorithme de recherche de chemin :

- `nbVehiculeAvailable`: Nombre de véhicules disponibles.
- `cost`: Coût associé à l'étiquette.
- `parent`: Indice du parent dans le graphe de recherche.
- `pos`: Position dans le graphe de recherche.

## ``T_INSTANCE``

Structure représentant une instance du Problème de Routage de Véhicules (VRP) :

- `nbClients`: Nombre de clients.
- `nbVehicle`: Nombre de véhicules.
- `capacityVehicle`: Capacité des véhicules.
- `dist[NMAX][MMAX]`: Matrice des distances entre chaque ville.
- `quantite[NMAX]`: Quantité demandée par chaque client.

## ``T_TOUR``

Structure représentant une tournée dans la solution du VRP :

- `nbClientsDeserved`: Nombre de clients desservis pendant la tournée.
- `cost`: Coût de la tournée.
- `clientsList[NMAX]`: Liste des clients desservis.
- `capacity[NMAX]`: Liste cumulative de la capacité pendant la tournée.

## ``T_SOLUTION``

Structure représentant la solution complète du VRP :

- `nbTours`: Nombre total de tournées.
- `tours[NMAX]`: Liste des tournées.
- `date[NMAX]`: Date d'arrivée pour chaque client.
- `parent[NMAX]`: Liste des parents.
- `cost`: Coût total de la solution.
- `vecteur[NMAX]`: Solution du Problème du Voyageur de Commerce (TSP).

